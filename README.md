# README #

Sunny iOS app.

### What is this repository for? ###

* This is a Swift project for the Sunny iOS app, presenting weather conditions in various locations
* Application is using OpenWeatherMap API (http://openweathermap.org/current) to fetch weather data
* 1.0

### How do I get set up? ###

* Install carthage (if not installed)
* In the project folder (root folder of the cloned repo) execute ```carthage update```