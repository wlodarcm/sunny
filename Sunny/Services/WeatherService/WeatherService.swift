//
//  WeatherService.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

protocol WeatherService {
    func getWeather(by location: GeoLocation, completionHandler: @escaping (Weather?) -> Void)
    func getWeather(byCityName cityName: String, completionHandler: @escaping (Weather?) -> Void)
}
