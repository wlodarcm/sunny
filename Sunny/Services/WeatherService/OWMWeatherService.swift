//
//  OWMWeatherService.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import UIKit

fileprivate enum ParamKey: String {
    case apiKey = "appid"
    case city = "q"
    case latitude = "lat"
    case longitude = "lon"
    case units = "units"
}

fileprivate enum TemperatureUnit: String {
    case celcius = "metric"
    case fahrenheit = "imperial"
}

fileprivate typealias Parameters = [ParamKey: Any]

class OWMWeatherService {
    
    // MARK: - Private properties
    fileprivate let networkClient: NetworkClient
    fileprivate let baseUrl = "http://api.openweathermap.org/data/2.5/weather"
    private let apiKey: String
    private let parser: JSONToDictionaryParser

    init(with networkClient: NetworkClient, parser: JSONToDictionaryParser, apiKey: String) {
        self.networkClient = networkClient
        self.apiKey = apiKey
        self.parser = parser
    }

    
    // MARK: - Private methods
    fileprivate func request(parameters: Parameters, completionHandler: @escaping (Data?) -> Void) {
        var params = parameters
        params[.apiKey] = apiKey
        params[.units] = TemperatureUnit.celcius.rawValue
        var requestParameters: RequestParameters? = nil
        requestParameters = params.map { (paramKey, value) in
            return (paramKey.rawValue, value)
        }
        networkClient.request(baseUrl, parameters: requestParameters, completionHandler: completionHandler)
    }
    
    
    fileprivate func parse(weatherData data: Data?) -> Weather? {
        guard let weatherJson = parser.parse(data) else {
            return nil
        }
        return try? OWMWeather(json: weatherJson)
    }
}

// MARK: - Extension for default configuration (just to safe time now instead of pushing dependencies)
extension OWMWeatherService {
    convenience init() {
        guard let apiKey = Bundle.main.object(forInfoDictionaryKey: "openweathermap.key") as? String else {
            fatalError("openweathermap.key is required but missing in .plist file")
        }
        self.init(with: AlamofireClient(), parser: StandardJSONToDictionaryParser(), apiKey: apiKey)
    }
}

extension OWMWeatherService: WeatherService {
    
    func getWeather(by location: GeoLocation, completionHandler: @escaping (Weather?) -> Void) {
        var params = Parameters()
        params[.latitude] = location.latitude
        params[.longitude] = location.longitude
        request(parameters: params) {
            [weak self] data in
            completionHandler(self?.parse(weatherData: data))
        }
    }
    
    func getWeather(byCityName cityName: String, completionHandler: @escaping (Weather?) -> Void) {
        var params = Parameters()
        params[.city] = cityName
        request(parameters: params) {
            [weak self] data in
            let weather = self?.parse(weatherData: data)
            completionHandler(weather)
        }
    }
}
