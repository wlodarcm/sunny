//
//  AlamofireClient.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireClient: NetworkClient {
    func request(_ urlString: String, parameters: Parameters?, completionHandler: @escaping (Data?) -> Void) {
        Alamofire.request(urlString, parameters: parameters).responseJSON { response in
            completionHandler(response.data)
        }
    }
}
