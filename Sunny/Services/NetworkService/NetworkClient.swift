//
//  NetworkClient.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

public typealias RequestParameters = [String: Any]

protocol NetworkClient {
    func request(_ urlString: String, parameters: RequestParameters?, completionHandler: @escaping (Data?) -> Void)
}
