//
//  JSONToDictionaryParser.swift
//  Sunny
//
//  Created by Marcin on 09/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

protocol JSONToDictionaryParser {
    func parse(_ data: Data?) -> [String: Any]?
}
