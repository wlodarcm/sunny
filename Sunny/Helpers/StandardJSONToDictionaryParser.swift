//
//  StandardJSONToDictionaryParser.swift
//  Sunny
//
//  Created by Marcin on 09/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

class StandardJSONToDictionaryParser: JSONToDictionaryParser {
    func parse(_ data: Data?) -> [String : Any]? {
        guard let data = data else {
            return nil
        }
        guard let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any] else {
            return nil
        }
        return json
    }
}
