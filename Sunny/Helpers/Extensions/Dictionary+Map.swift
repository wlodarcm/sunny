//
//  Dictionary+Map.swift
//  Sunny
//
//  Created by Marcin on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

extension Dictionary {
    func map<K : Hashable, V>(transform: (Key, Value) -> (K, V)) -> [K:V] {
        var result: [K:V] = [:]
        for keyValue in self {
            let transformed = transform(keyValue.key, keyValue.value)
            result[transformed.0] = transformed.1
        }
        return result
    }
}
