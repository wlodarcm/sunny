//
//  SerializationError.swift
//  Sunny
//
//  Created by Marcin on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

enum SerializationError: Error {
    case missing(String)
    case invalid(String, Any)
}
