//
//  OWMWeather.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import UIKit

/** 
 Structure represents deserialised object received from OpenWeatherMap (http://openweathermap.org/current)
 Not the entire openweathermap weather object has been deserialized because of the time constraints
 
 Some additional fields were deserialized compering to Weather protocol - just to show
 that OWMWeather struct can have more properties than specified in Weather protocol
 */
struct OWMWeather {
    var sunrise: Date
    var sunset: Date
    var description: String
    var temperature: Double
    var pressure: Int
    var humidity: Int
    var image: UIImage?
}

extension OWMWeather: Weather {}

extension OWMWeather {
    init(json: [String: Any]) throws {
        guard let sys = json["sys"] as? [String: Any] else {
            throw SerializationError.missing("sys")
        }
        
        guard let sunrise = sys["sunrise"] as? TimeInterval else {
            throw SerializationError.missing("sunrise")
        }
        
        guard let sunset = sys["sunset"] as? TimeInterval else {
            throw SerializationError.missing("sunset")
        }
        
        guard let weathers = json["weather"] as? [Any],
            let weather = weathers.first as? [String: Any] else {
            throw SerializationError.missing("weather")
        }
        
        guard let description = weather["description"] as? String else {
            throw SerializationError.missing("description")
        }
        
        guard let main = json["main"] as? [String: Any] else {
            throw SerializationError.missing("main")
        }
        
        guard let temperature = main["temp"] as? Double else {
            throw SerializationError.missing("temp")
        }
        
        guard let pressure = main["pressure"] as? Int else {
            throw SerializationError.missing("pressure")
        }
        
        guard let humidity = main["humidity"] as? Int else {
            throw SerializationError.missing("humidity")
        }
        
        self.sunrise = Date(timeIntervalSince1970: sunrise)
        self.sunset = Date(timeIntervalSince1970: sunset)
        self.description = description
        self.temperature = temperature
        self.pressure = pressure
        self.humidity = humidity
    }
}
