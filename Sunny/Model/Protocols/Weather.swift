//
//  Weather.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation
import UIKit

protocol Weather {
    var sunrise: Date { get }
    var sunset: Date { get }
    var description: String { get }
    var image: UIImage? { get }
}
