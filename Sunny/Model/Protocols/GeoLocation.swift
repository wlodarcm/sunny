//
//  GeoLocation.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

protocol GeoLocation {
    var latitude: Double { get }
    var longitude: Double { get }
}
