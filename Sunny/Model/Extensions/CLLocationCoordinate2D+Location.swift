//
//  CLLocationCoordinate2D+Location.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationCoordinate2D: GeoLocation {}
