//
//  NetworkWeatherViewModel.swift
//  Sunny
//
//  Created by Marcin on 09/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

class NetworkWeatherViewModel: WeatherViewModel {
    
    // MARK: Private properties
    private let weatherService: WeatherService

    // MARK: Internal properties
    var location: Location
    
    init(weatherService: WeatherService = OWMWeatherService(), location: Location) {
        self.weatherService = weatherService
        self.location = location
    }
    
    // MARK: Public methods
    
    func refreshWeather(completionHandler: @escaping (Weather?) -> Void) {
        weatherService.getWeather(byCityName: location.name, completionHandler: completionHandler)
    }
    
    
}
