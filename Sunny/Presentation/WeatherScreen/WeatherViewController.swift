//
//  WeatherViewController.swift
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var navItem: UINavigationItem!

    var viewModel: WeatherViewModel? {
        didSet {
            navItem.title = viewModel?.location.name
            viewModel?.refreshWeather {
                [weak self] weather in
                guard let weather = weather else {
                    return
                }
                self?.configure(with: weather)
            }
        }
    }

    private func configure(with weather: Weather) {
        detailDescriptionLabel.text = "\(weather)"
    }
}
