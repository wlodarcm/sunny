//
//  WeatherViewModel.swift
//  Sunny
//
//  Created by Marcin on 09/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

protocol WeatherViewModel {
    var location: Location { get }
    func refreshWeather(completionHandler: @escaping (Weather?) -> Void)
}
