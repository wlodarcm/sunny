//
//  Location.swift
//  Sunny
//
//  Created by Marcin on 09/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

protocol Location {
    var name: String { get }
}

extension String: Location {
    var name: String {
        return self
    }
}
