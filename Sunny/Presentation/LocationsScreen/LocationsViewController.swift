//
//  LocationsViewController
//  Sunny
//
//  Created by Marcin Wlodarczyk on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import UIKit

class LocationsViewController: UITableViewController {

    var detailViewController: WeatherViewController? = nil
    var viewModel: LocationsViewModel? {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? WeatherViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            guard  let indexPath = self.tableView.indexPathForSelectedRow,
                let viewModel = viewModel,
                let location = viewModel.location(at: indexPath.row) else {
                    return
            }
            
            
            let controller = (segue.destination as! UINavigationController).topViewController as! WeatherViewController
            controller.viewModel = NetworkWeatherViewModel(location: location)
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    
    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        return viewModel.numberOfLocations()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        guard let viewModel = viewModel else {
            cell.textLabel!.text = ""
            return cell
        }
        
        let location = viewModel.location(at: indexPath.row)
        // TODO: cell should have a configure method that receives an object conforming to CellDelegate
        
        cell.textLabel!.text = location?.name
        return cell
    }
}

