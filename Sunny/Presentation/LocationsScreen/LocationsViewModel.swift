//
//  LocationsViewModel.swift
//  Sunny
//
//  Created by Marcin on 09/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation

protocol LocationsViewModel {
    func numberOfLocations() -> Int
    func location(at index: Int) -> Location?
}


/**
 * NOTE: for the simplicity, since we don't need any data modification,
 * we can use Array as a viewModel for LocationsViewController
 */
extension Array: LocationsViewModel {
    func numberOfLocations() -> Int {
        return count
    }
    
    func location(at index: Int) -> Location? {
        guard indices.contains(index) else {
            return nil
        }
        guard let location = self[index] as? Location else {
            return nil
        }
        return location
    }
}
