//
//  OWMWeatherServiceSpec.swift
//  SunnyTests
//
//  Created by Marcin on 08/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Quick
import Nimble
@testable import Sunny

class OWMWeatherServiceSpec: QuickSpec {

    override func spec() {
        describe("OWMWeatherService") {

            var sut: OWMWeatherService!
            var mockClient: NetworkClientMock!
            let apiKey = "Fixture Api Key"
            
            beforeEach {
                mockClient = NetworkClientMock()
                sut = OWMWeatherService(with: mockClient, apiKey: apiKey)
            }
            
            describe("when sending request for weather for some city") {
                
                var params: RequestParameters!
                let expectedCity = "Fixture City"
                
                beforeEach {
                    sut.getWeather(by: expectedCity) { _ in }
                    params = mockClient.receivedParameters
                }
                
                it("should set the proper base url") {
                    let expectedBaseUrl = "http://api.openweathermap.org/data/2.5/weather"
                    expect(mockClient.receivedBaseUrl).to(equal(expectedBaseUrl))
                }
                
                it("should set all the required parameters") {
                    expect(params).to(haveCount(3))
                    expect(params.keys).to(contain("appid"))
                    expect(params.keys).to(contain("units"))
                    expect(params.keys).to(contain("q"))
                }
                
                it("should set the proper apiKey parameter") {
                    let receivedApiKey = params["appid"] as! String
                    expect(receivedApiKey).to(equal(apiKey))
                }
                
                it("should set the proper units parameter") {
                    let receivedUnit = params["units"] as! String
                    let expectedUnit = "metric"
                    expect(receivedUnit).to(equal(expectedUnit))
                }
                
                it("should set the proper city parameter") {
                    let receivedCity = params["q"] as! String
                    expect(receivedCity).to(equal(expectedCity))
                }
            }
            
        }
    }
}

class NetworkClientMock: NetworkClient {
    
    var receivedParameters: RequestParameters?
    var receivedBaseUrl: String?
    var dataToBeReturned: Data?
    
    func request(_ urlString: String, parameters: RequestParameters?, completionHandler: @escaping (Data?) -> Void) {
        receivedParameters = parameters
        receivedBaseUrl = urlString
        
        completionHandler(dataToBeReturned)
    }
}
