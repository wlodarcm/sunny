//
//  OWMWeatherSpec.swift
//  Sunny
//
//  Created by Marcin on 09/02/2017.
//  Copyright © 2017 MarcinWlodarczyk. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import Sunny

class OWMWeatherSpec: QuickSpec {
    
    override func spec() {
        describe("OWMWeather") {
            
            var sut: OWMWeather?
            
            context("when initialized with json object") {
                
                beforeEach {
                    let weatherJsonData = [String: Any]()
                    sut = try? OWMWeather(json: weatherJsonData)
                }
                
                it("should the instance be initialised") {
                    expect(sut).notTo(beNil())
                }
                
                it("should set the properteis properly") {
                    //... time :(
                }
                
                // ... time :(
            }
            
        }
    }
}
